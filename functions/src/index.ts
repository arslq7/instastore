import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const userSignup = functions.auth.user().onCreate( user => {
    return admin.firestore().collection('users').doc(user.uid).set({
        username: user.email,
        bookMarked: [],
        purchaseHistory: [],
        wishList: [],
        cart: [],
        storeId: '',
        imageURL: '',
        following: ''
    });
});

export const userDelete = functions.auth.user().onDelete( user => {
    const doc = admin.firestore().collection('users').doc(user.uid);
    return doc.delete();
});
