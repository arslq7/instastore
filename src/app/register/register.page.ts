import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common'
import { ToastyService } from '../services/toasty.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  email: string;
  password: string;
  cpassword: string;
  constructor(
    private afAuth: AngularFireAuth,
    private toast: ToastyService,
    private location: Location,
    private navCtrl: NavController,
    private Slogin: LoginService
  ) { }

  ngOnInit() {
  }

  async register() {
    const { email, password, cpassword } = this;
    if (password === cpassword) {
      try {
        const res = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
        this.toast.showAlert( 'register Succesfull', 'success' );
        this.Slogin.loginStatus = true;
        this.navCtrl.navigateRoot('home');

      } catch (err) {
        this.toast.showAlert( err.message, 'danger' );
      }

    } else {
      this.toast.showAlert('Password did not match', 'danger');
    }

  }

   async googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    try {
      await firebase.auth().signInWithPopup(provider)
          .then((user) => {
            console.log(user);
            this.Slogin.loginStatus = true;
            this.toast.showAlert('Login Successfull', 'success');
            this.navCtrl.navigateRoot('home');
          });
    } catch (error) {
      this.toast.showAlert(error.message, 'danger');
    }
  }

  back() {
    this.location.back();
    // this.router.navigateByUrl('account');
  }
}
