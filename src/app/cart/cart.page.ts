import { Component, OnInit } from '@angular/core';
import {CartService} from '../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  items = [];
  constructor(
      public cart: CartService
  ) { }

  ngOnInit() {
    this.items = this.cart.items;
  }
  delItem(product) {
    this.cart.deleteItem(product);
  }
  changeQuantity(product, action) {
    this.cart.changeQuantity(product, action);
  }
}
