import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as firebase from 'firebase';
import {CartService} from '../services/cart.service';

@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.page.html',
  styleUrls: ['./productdetails.page.scss'],
})
export class ProductdetailsPage implements OnInit {
  pid: any;
  product: any;
  db = firebase.firestore();
  owner: any;
  constructor(
      private activatedRoute: ActivatedRoute,
      public cart: CartService
  ) { }

  ngOnInit() {
    this.pid = this.activatedRoute.snapshot.paramMap.get('pid');
    console.log(this.pid);
    this.getProduct();
  }
  getProduct() {
    this.db.collection('post').doc(this.pid).get().then(doc => {
      const data = doc.data();
      this.product = data;
      const storage = firebase.storage();
      const storageRef = storage.ref();
      const ref = storageRef.child(data.image);
      ref.getDownloadURL().then(url => {
        data.image = url;
        this.product = data;
      });
      this.db.collection('store').doc(this.product.storeId).get().then(doc1 => {
        const data1 = doc1.data();
        this.owner = data1;
        const storage1 = firebase.storage();
        const storageRef1 = storage1.ref();
        const ref1 = storageRef1.child(data1.storeImage);
        ref1.getDownloadURL().then(url1 => {
          data1.storeImage = url1;
          this.owner = data1;
        });
      });
      console.log(this.product);
    });
  }
  addItem(product, owner) {
    this.cart.addItem(product, owner);
  }
}
