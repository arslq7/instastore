import * as firebase from 'firebase';

const config = {
    apiKey: 'AIzaSyA_xAXNk-WMfxZN6vZSeDZsepiQkZ-ne2g',
    authDomain: 'instashop-5d66a.firebaseapp.com',
    databaseURL: 'https://instashop-5d66a.firebaseio.com',
    projectId: 'instashop-5d66a',
    storageBucket: 'instashop-5d66a.appspot.com',
    messagingSenderId: '1007625491224',
    appId: '1:1007625491224:web:1195ed6564b938a946446d',
    measurementId: 'G-BR6N417PLF'
  };
firebase.initializeApp(config);

export default config;
