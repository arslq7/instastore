import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {ToastyService} from '../services/toasty.service';
import {LoginService} from '../services/login.service';
import * as firebase from 'firebase';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

    email: string;
    password: string;
    user: any;
    loginStatus: any;
    userData: any;
    imageUrl: any;
    imageRef: any;
    db = firebase.firestore();
    accountImageCheck;

    constructor(
        private Slogin: LoginService,
        private router: Router,
        private afAuth: AngularFireAuth,
        public toast: ToastyService,
        private navCtrl: NavController
    ) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.loginStatus = this.Slogin.loginStatus;
        if (this.loginStatus) {
            this.user = firebase.auth().currentUser;
            this.getUserData();
        }
    }

    register() {
        this.router.navigateByUrl('account/register');
    }

    async login() {
        const {email, password} = this;
        try {
            const res = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
            this.toast.showAlert('Login Succefull', 'success');
            this.Slogin.loginStatus = true;
            this.navCtrl.navigateRoot('home');
        } catch (err) {
            this.toast.showAlert(err.message, 'danger');
            console.dir(err);
        }
    }

    logout() {
        firebase.auth().signOut();
        this.toast.showAlert('Logout Succesfull', 'success');
        this.loginStatus = false;
    }

    getUserData() {
        this.db.collection('users').doc(this.user.uid)
            .onSnapshot(async doc => {
               this.userData = doc.data();
               if (this.userData.imageURL) {
                   const storage = firebase.storage();
                   const storeageRef = storage.ref();
                   this.imageRef = storeageRef.child(this.userData.imageURL);
                   await this.imageRef.getDownloadURL().then(url => {
                       this.imageUrl = url;
                       this.accountImageCheck = true;
                       console.log(this.imageUrl);
                   });
               } else {
                   this.accountImageCheck = false;
               }
            });
    }
    editProfile() {
        this.router.navigateByUrl('account/edit');
    }
    store() {
        this.router.navigateByUrl('account/store');
    }
}
