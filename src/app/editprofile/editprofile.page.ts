import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import * as firebase from 'firebase';
import * as $ from 'jquery';
import {ToastyService} from '../services/toasty.service';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {
  db = firebase.firestore();
  userData: any;
  user: any;
  name;
  path: any;
  constructor(
      private location: Location,
      private toast: ToastyService
  ) { }

  ngOnInit() {
    this.user =  firebase.auth().currentUser;
    this.getUserData();
  }

  ionViewDidEnter() {
   //
  }
  back() {
    this.location.back();
  }
  async getUserData() {
    await this.db.collection('users').doc(this.user.uid).get().then(doc => {
      this.userData = doc.data();
      this.name = this.userData.username;
      console.log(this.userData);
    });
  }
  update() {
    if (this.name === '') {
      this.toast.showAlert('Username is empty', 'danger');
    } else {
      if (this.path) {
        this.uploadImage(this.path);
      }
      const name = this.db.collection('users').doc(this.user.uid).update({
        username: this.name
      }).then(() => {
        this.toast.showAlert('Profile Updated', 'success');
        this.location.back();
      });
    }
  }
  uploadImage(path) {
    const files = path;
    this.db.collection('users').doc(this.user.uid).update({
      imageURL: 'profile/' + files.name
    });
    // this.user.updateProfile({
    //   imageURL: 'profile/' + files.name
    // });
    const storageRef = firebase.storage().ref('profile/' + files.name);
    storageRef.put(files);
  }
  readURL(input) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
      $('#imagePreview').hide();
      $('#imagePreview').fadeIn(650);
      this.path = input.target.files[0];
    };
    reader.readAsDataURL(input.target.files[0]);
  }
}
