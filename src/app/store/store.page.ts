import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import * as firebase from 'firebase';
import {Router} from '@angular/router';
import {ToastyService} from '../services/toasty.service';
import {LoadingController, NavController} from '@ionic/angular';
import {CacheService} from 'ionic-cache';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-store',
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.scss'],
})
export class StorePage implements OnInit {
  user: any;
  userData: any;
  db = firebase.firestore();
  store: any = '';
  check;
  category: any;
  storeName: any;
  stripeKey: any;
  files: any;
  storeData: any;
  imageUrl: any;
  imageRef: any;
  imageCheck;
  posts = [];
  showLoading = true;
  constructor(
      private location: Location,
      private router: Router,
      public toast: ToastyService,
      private navCtrl: NavController,
      public loadingController: LoadingController,
      private cache: CacheService
  ) { }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }

  ngOnInit() {
    // this.presentLoading();
    this.user = firebase.auth().currentUser;
    this.getUserData();
  }
  ionViewDidEnter() {
    // this.user = firebase.auth().currentUser;
    // this.getUserData();
  }
  async getUserData() {
    await this.db.collection('users').doc(this.user.uid).get().then(async doc => {
      this.userData = doc.data();
      this.store = this.userData.storeId;
      this.check = !this.store;
      this.db.collection('store').doc(this.userData.storeId).get().then(async doc1 => {
        this.getPosts();
        this.storeData = doc1.data();
        const storage = firebase.storage();
        const storeageRef = storage.ref();
        if (this.storeData.storeImage) {
          this.imageRef = storeageRef.child(this.storeData.storeImage);
          await this.imageRef.getDownloadURL().then(url => {
            this.imageUrl = url;
            this.imageCheck = true;
          });
        } else {
          this.imageCheck = false;
        }
        console.log(this.storeData);
        this.showLoading = false;
      });
    });
  }
  uploadImage(event) {
    this.files = event.target.files[0];
  }
  getPosts() {
     this.db.collection('post').where('storeId', '==', this.userData.storeId)
        .onSnapshot((snapshot) => {
          snapshot.forEach((doc) => {
            const data = doc.data();
            const storage = firebase.storage();
            const storeageRef = storage.ref();
            const ref = storeageRef.child(data.image);
            ref.getDownloadURL().then(url => {
              data.image = url;
              data.id = doc.id;
              this.posts.push(data);
            });
          });
          console.log(this.posts);
        });
  }
  buyStore() {
    if (this.storeName !== '' && this.category !== '' && this.stripeKey !== '') {
      const ID = firebase.firestore().collection('store').add({
        category: this.category,
        name: this.storeName,
        followers: [],
        posts: [],
        storeImage: '',
        stripeKey: this.stripeKey
      }).then((doc) => {
        if (this.files !== '') {
          this.db.collection('store').doc(doc.id).update({
            storeImage: 'storeProfile/' + this.files.name
          }).then(() => {
            const storageRef = firebase.storage().ref('storeProfile/' + this.files.name);
            storageRef.put(this.files);
          });
        }
        firebase.firestore().collection('users').doc(this.user.uid).update({
          storeId: doc.id
        });
      });
    } else {
      this.toast.showAlert('Please input data', 'danger');
    }
  }
  details(id: any) {
    this.router.navigateByUrl('account/store/productdetails/' + id);
  }
  back() {
    this.location.back();
  }
  addProduct() {
    this.router.navigateByUrl('account/store/addproduct/' + this.storeData.category + '/' + this.userData.storeId);
  }
}
