import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import {LoginService} from '../services/login.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  constructor(
    private router: Router,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }


  homePage() {
    this.navCtrl.navigateRoot('home');
  }
  cartPage() {
    this.navCtrl.navigateRoot('cart');
  }
  accountPage() {
    this.navCtrl.navigateRoot('account');
  }
  searchPage() {
    this.navCtrl.navigateRoot('search');
  }
}
