import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-searchlist',
  templateUrl: './searchlist.page.html',
  styleUrls: ['./searchlist.page.scss'],
})
export class SearchlistPage implements OnInit {
  category: any;
  stores = [];
  constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit() {
    this.category = this.activatedRoute.snapshot.paramMap.get('category');
    console.log(this.category);
    this.getStores();
    console.log(this.stores);
  }
  getStores() {
    firebase.firestore().collection('store').where('category', '==', this.category).get().then(query => {
      query.forEach(doc => {
        const data = doc.data();
        data.id = doc.id;
        const storage = firebase.storage();
        const storeageRef = storage.ref();
        const ref = storeageRef.child(data.storeImage);
        ref.getDownloadURL().then(url => {
          data.storeImage = url;
          this.stores.push(data);
        });
      });
      console.log(this.stores);
    });
  }
  storedetails(id) {
    this.router.navigateByUrl('home/storeview/' + id);
  }
}
