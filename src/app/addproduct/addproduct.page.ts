import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import * as firebase from 'firebase';
import * as $ from 'jquery';
import {ToastyService} from '../services/toasty.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.page.html',
  styleUrls: ['./addproduct.page.scss'],
})
export class AddproductPage implements OnInit {
  category: any;
  storeId: any;
  name: any;
  description: any;
  discount = 0;
  image: any;
  price: any;
  constructor(
      private activatedRoute: ActivatedRoute,
      private location: Location,
      private toast: ToastyService
  ) { }

  ngOnInit() {
    this.category = this.activatedRoute.snapshot.paramMap.get('category');
    this.storeId = this.activatedRoute.snapshot.paramMap.get('storeId');
    console.log(this.category);
    console.log(this.storeId);
  }
  back() {
    this.location.back();
  }
  // uploadImage(event) {
  //   this.image = event.target.files[0];
  // }
  addProduct() {
    if (this.name !== '' && this.description !== '' && this.price !== ''  && this.image !== '') {
      firebase.firestore().collection('post').add({
        name: this.name,
        description: this.description,
        discount: this.discount,
        image: 'postImage/' + this.image.name,
        price: this.price,
        storeId: this.storeId,
        category: this.category
      }).then(doc => {
        const storageRef = firebase.storage().ref('postImage/' + this.image.name);
        storageRef.put(this.image);
        this.toast.showAlert('Product Added', 'success');
        this.location.back();
      });
    } else {
      this.toast.showAlert('Please input data', 'danger');
    }
  }
  readURL(input) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
      $('#imagePreview').hide();
      $('#imagePreview').fadeIn(650);
      this.image = input.target.files[0];
    };
    reader.readAsDataURL(input.target.files[0]);
  }
}
