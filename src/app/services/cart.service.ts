import { Injectable } from '@angular/core';
import {ToastyService} from './toasty.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];
  constructor(
      private toasty: ToastyService
  ) { }
  addItem(data: [], owner: []) {
    const item = {quantity: 1, item: data, owner};
    this.items.push(item);
    this.toasty.showAlert('Item Added', 'success');
  }
  deleteItem(item: any) {
    this.items.splice(this.items.indexOf(item), 1);
    this.toasty.showAlert('Item deleted', 'success');
  }
  changeQuantity(item, action) {
    if (action === 'increase') {
      this.items[this.items.indexOf(item)].quantity += 1;
    } else if (action === 'decrease') {
      if (this.items[this.items.indexOf(item)].quantity > 1) {
        this.items[this.items.indexOf(item)].quantity -= 1;
      }
    }
  }
}
