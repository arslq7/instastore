import { Injectable } from '@angular/core';
import { ToastController  } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastyService {

  constructor(
    public toast: ToastController ,
  ) { }

  async showAlert(message: string, color) {
    const alert = await this.toast.create ({
      color,
      message,
      position: 'top',
      duration: 1500
    });

    await alert.present();
  }
}
