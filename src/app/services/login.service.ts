import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginStatus: boolean;
  user: any;
  constructor() { }
   checkLoginStatus() {
     firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.user = user;
        this.loginStatus = true;
      } else {
        this.loginStatus = false;
      }
      console.log(this.loginStatus);
    });
  }
}
