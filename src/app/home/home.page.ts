import { Component, OnInit } from '@angular/core';
import {LoginService} from '../services/login.service';
import * as firebase from 'firebase';
import {Router} from '@angular/router';
import {CartService} from '../services/cart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  products = [];
  db = firebase.firestore();
  owners = [];
  constructor(
      private Slogin: LoginService,
      private router: Router,
      private cart: CartService
  ) { }

  ngOnInit() {
    this.getProduct();
  }
  refresh(refresher) {
    this.getProduct();
    refresher.target.complete();
  }
  getProduct() {
    this.db.collection('post').get().then(snapshot => {
      this.owners = [];
      this.products = [];
      snapshot.forEach(doc => {
        const data = doc.data();
        const storage = firebase.storage();
        const storageRef = storage.ref();
        const ref = storageRef.child(data.image);
        ref.getDownloadURL().then(url => {
          data.image = url;
          data.id = doc.id;
        });
        this.db.collection('store').doc(data.storeId).get().then(doc1 => {
          const data1 = doc1.data();
          const storage1 = firebase.storage();
          const storageRef1 = storage1.ref();
          const ref1 = storageRef1.child(data1.storeImage);
          ref1.getDownloadURL().then(url1 => {
            data1.storeImage = url1;
            this.products.push(data);
            this.owners.push(data1);
          });
        });
      });
      console.log(this.products);
      console.log(this.owners);
    });
  }
  getDetails(id) {
    this.router.navigateByUrl('home/storeview/' + id);
  }
  addItem(data, index) {
    this.cart.addItem(data, this.owners[index]);
  }
}
