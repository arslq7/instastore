import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StoreviewPage } from './storeview.page';

describe('StoreviewPage', () => {
  let component: StoreviewPage;
  let fixture: ComponentFixture<StoreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StoreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
