import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoreviewPage } from './storeview.page';

const routes: Routes = [
  {
    path: '',
    component: StoreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoreviewPageRoutingModule {}
