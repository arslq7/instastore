import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StoreviewPageRoutingModule } from './storeview-routing.module';

import { StoreviewPage } from './storeview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoreviewPageRoutingModule
  ],
  declarations: [StoreviewPage]
})
export class StoreviewPageModule {}
