import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-storeview',
  templateUrl: './storeview.page.html',
  styleUrls: ['./storeview.page.scss'],
})
export class StoreviewPage implements OnInit {
  storeId: any;
  db = firebase.firestore();
  storeData: any;
  posts = [];
  constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit() {
    this.storeId = this.activatedRoute.snapshot.paramMap.get('storeId');
    this.getUserData();
    console.log(this.storeId);
  }
  async getUserData() {
    await this.db.collection('store').doc(this.storeId).get().then(doc => {
      this.storeData = doc.data();
      this.getPosts();
      const storage = firebase.storage();
      const storeageRef = storage.ref();
      if (this.storeData.storeImage) {
        const imageRef = storeageRef.child(this.storeData.storeImage);
        imageRef.getDownloadURL().then(url => {
          this.storeData.storeImage = url;
        });
      }
      console.log(this.storeData);
    });
  }
  getPosts() {
    this.db.collection('post').where('storeId', '==', this.storeId)
        .onSnapshot((snapshot) => {
          snapshot.forEach((doc) => {
            const data = doc.data();
            const storage = firebase.storage();
            const storeageRef = storage.ref();
            const ref = storeageRef.child(data.image);
            ref.getDownloadURL().then(url => {
              data.image = url;
              data.id = doc.id;
              this.posts.push(data);
            });
          });
          console.log(this.posts);
        });
  }
  details(id) {
    this.router.navigateByUrl('account/store/productdetails/' + id);
  }
}
